const restify = require('restify');
const routes = require('../app/api/routes/routes');

const app = restify.createServer();
app.use(restify.plugins.acceptParser(app.acceptable));
app.use(restify.plugins.bodyParser());
app.use(restify.plugins.queryParser());

routes(app);

module.exports = app;