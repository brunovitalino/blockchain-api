'use strict'
const { TransactionHandler } = require('sawtooth-sdk/processor/handler');
const { Decoder } = require('cbor')

const MovimentacaoService = require('../api/services/movimentacao-service');

// Encoding helpers and constants
const encode = obj => Buffer.from(JSON.stringify(obj, Object.keys(obj).sort()))

const movimentacaoService = new MovimentacaoService();

class MovimentacaoHandler extends TransactionHandler {
  constructor(info) {
    console.log('Iniciando smart contract para Movimentacoes ')
    super(info.family, [info.version], [info.prefix]);
  }

  apply(txn, context) {
    console.log("Chegando uma nova transacao");

    const dataDecoded = Decoder.decodeFirstSync(txn.payload);
    const payload = JSON.parse(dataDecoded);

    payload.familyName = 'tacografo';
    const blockAddress = movimentacaoService.generateMovimentacaoAddress(payload)

    const { viagemId, horario, velocidade } = payload;

    return context.setState({
      [blockAddress]: encode({ viagemId, horario, velocidade })
    });
  }
}

module.exports = MovimentacaoHandler;
