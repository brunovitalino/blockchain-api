const request = require('request');

class SawtoothDao {

  searchBlockchain(address, callback) {
    request({
      url: `http://localhost:8008/state?address=${address}`,
      method: 'GET',
      headers: { 'Content-Type': 'application/json' }
    }, (error, response, body) => {
      if (error) {
        console.log('ERRO', error);
      } else {
        const items = JSON.parse(response.body).data;
        const decodedInfo = items
          .map(item => JSON.parse(new Buffer(item.data, 'base64').toString()));
        callback(decodedInfo);
      }
    });
  }
  
  sendToSawtoothApi(batchBytes, callback) {
    request({
      url: 'http://localhost:8008/batches?wait',
      method: 'POST',
      body: batchBytes,
      encoding: null,
      headers: { 'Content-Type': 'application/octet-stream' }
    }, (error, response, body) => {
      if (error) {
        console.log('ERRO', error);
      } else {
        const body = JSON.parse(new Buffer(response.body, 'base64').toString());
        callback(body);
      }
    })
  }

}

module.exports = SawtoothDao;