'use strict'
const { createContext, CryptoFactory } = require('sawtooth-sdk/signing');
const { Secp256k1PrivateKey } = require('sawtooth-sdk/signing/secp256k1');
const { protobuf } = require('sawtooth-sdk');
const { createHash } = require('crypto');

const TacografoUtil = require('../utils/tacografo-util');

class SawtoothUtil {

  static getHandlerInfo(familyName) {
    return {
      prefix: TacografoUtil.getAddress(familyName, 6),
      family: familyName,
      version: '0.0.1'
    };
  }

  static buildSawtoothPackage(payloadBytes, privateKey) {

    const context = createContext('secp256k1');
    const privateKeyInstance = Secp256k1PrivateKey.fromHex(privateKey);
    const signer = new CryptoFactory(context).newSigner(privateKeyInstance);
  
    const { family, version, prefix } = this.getHandlerInfo('tacografo');
  
    const transactionHeaderBytes = protobuf.TransactionHeader.encode({
      familyName: family,
      familyVersion: version,
      inputs: [prefix],
      outputs: [prefix],
      signerPublicKey: signer.getPublicKey().asHex(),
      batcherPublicKey: signer.getPublicKey().asHex(),
      payloadSha512: createHash('sha512').update(payloadBytes).digest('hex')
    }).finish();
  
    const signature = signer.sign(transactionHeaderBytes);
  
    const transaction = protobuf.Transaction.create({
      header: transactionHeaderBytes,
      headerSignature: signature,
      payload: payloadBytes
    });
  
    const batchHeaderBytes = protobuf.BatchHeader.encode({
      signerPublicKey: signer.getPublicKey().asHex(),
      transactionIds: [transaction.headerSignature],
    }).finish();
  
    const batchSignature = signer.sign(batchHeaderBytes);
  
    const batch = protobuf.Batch.create({
      header: batchHeaderBytes,
      headerSignature: batchSignature,
      transactions: [transaction]
    });
  
    const batchBytes = protobuf.BatchList.encode({
      batches: [batch]
    }).finish();
  
    return batchBytes;
  }

}

module.exports = SawtoothUtil;