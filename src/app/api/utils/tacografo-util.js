const { createHash } = require('crypto');

class TacografoUtil {

  static getAddress(key, length) {
    return createHash('sha512').update(key).digest('hex').slice(0, length);
  }

}

module.exports = TacografoUtil;