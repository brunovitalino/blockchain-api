const SawtoothUtil = require('../utils/sawtooth-util');
const SawtoothDao = require('../daos/sawtooth-dao');
const cbor = require('cbor');

const sawtoothDao = new SawtoothDao();

class SawtoothService {

  findAll(address, callback) {
    sawtoothDao.searchBlockchain(address, payload => {
      callback(payload);
    });
  }

  save(payload, callback) {
    console.log(payload);
    const payloadBytes = cbor.encode(JSON.stringify(payload));
    
    const batchBytes = SawtoothUtil.buildSawtoothPackage(payloadBytes, payload.registerCode);
    payload.registerCode = "";

    sawtoothDao.sendToSawtoothApi(batchBytes, body => {
      callback(body);
    });
  }

}

module.exports = SawtoothService;