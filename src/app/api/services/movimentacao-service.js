'use strict'
const SawtoothService = require('./sawtooth-service');
const TacografoUtil = require('../utils/tacografo-util');
const SawtoothUtil = require('../utils/sawtooth-util');

const sawtoothService = new SawtoothService();

class MovimentacaoService {

  generateMovimentacaoAddress(payload) {
    return SawtoothUtil.getHandlerInfo(payload.familyName).prefix + TacografoUtil.getAddress(payload.viagemId, 20)
      + TacografoUtil.getAddress(payload.registerCode, 20) + TacografoUtil.getAddress(payload.address, 24);
  }

  getMovimentacaoAddress(payload) {
    return SawtoothUtil.getHandlerInfo(payload.familyName).prefix + (
      payload.viagemId ? TacografoUtil.getAddress(payload.viagemId, 20) + (
        payload.registerCode ? TacografoUtil.getAddress(payload.registerCode, 20) + (
          payload.address ? TacografoUtil.getAddress(payload.address, 20) : ''
        ) : ''
      ) : ''
    );
  }

  findAll(parametros, callback) {
    const movimentacaoAddress = this.getMovimentacaoAddress(parametros);
    sawtoothService.findAll(movimentacaoAddress, movimentacoes => {
      callback(movimentacoes);
    });
  }

  save(movimentacao, callback) {
    sawtoothService.save(movimentacao, body => {
      movimentacao.link = body.link;
      callback(movimentacao);
    });
  }

}

module.exports = MovimentacaoService;