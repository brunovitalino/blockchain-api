const MovimentacaoService = require('../services/movimentacao-service');
const movimentacaoService = new MovimentacaoService();

class MovimentacaoController {

  static routes() {
      return {
        movimentacoes: 'movimentacoes',
        movimentacoesWithId: 'movimentacoes/:id'
      }
  }

  findAll() {
    return function(req, res, next) {
      console.log("ACESSOU O METODO");
      const payload = {};
      payload.familyName = req.params.familyName;
      payload.viagemId = req.query.viagemId;
      payload.registerCode = req.query.registerCode;
      payload.address = req.query.address;
      switch(req.params.familyName) {
        case 'tacografo':
          movimentacaoService.findAll(payload, movimentacoes => {
            res.send(movimentacoes);
            next();
          });
        default:
      }
    }
  }

  save() {
    return function(req, res, next) {
      const payload = req.body;
      movimentacaoService.save(payload, movimentacao => {
        res.send(201, movimentacao);
        next();
      });
    }
  }

}

module.exports = MovimentacaoController;