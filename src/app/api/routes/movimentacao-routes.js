const MovimentacaoController = require("../controllers/movimentacao-controller");

module.exports = (router) => {
  const movimentacaoController = new MovimentacaoController();
  const movimentacaoRoutes = MovimentacaoController.routes();
   
  router.get(movimentacaoRoutes.movimentacoes, movimentacaoController.findAll());
  router.post(movimentacaoRoutes.movimentacoes, movimentacaoController.save());
}