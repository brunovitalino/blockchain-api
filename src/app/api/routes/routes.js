var Router = require('restify-router').Router;
var router = new Router();

const movimentacaoRoutes = require('./movimentacao-routes');

module.exports = (server) => {
  movimentacaoRoutes(router);

  router.applyRoutes(server, '/blockchain/:familyName/api/');
}