const server = require('./src/config/custom-restify');
const processor = require('./src/app/sawtooth/processor');
const MovimentacaoHandler = require('./src/app/sawtooth/movimentacaoHandler');
const SawtoothUtil = require('./src/app/api/utils/sawtooth-util');

processor(new MovimentacaoHandler(SawtoothUtil.getHandlerInfo('tacografo')));

server.listen(8084, function() {
  console.log('%s listening at %s', server.name, server.url);
});
